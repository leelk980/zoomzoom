import { GlobalException, UserRoleEnum } from '@libs/common';
import { ClsManager } from '@libs/infrastructure/manager';
import { Auth } from '@libs/middleware/auth.guard';
import { TypedException, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';
import { CreateOneReservationPort } from './reservation/create-one-reservation';
import { CreateOneTourPort } from './tour/create-one-tour';
import { SignUpUserPort } from './user/sign-up-user';

@Controller('/_')
export class AppController {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly signUpUserPort: SignUpUserPort,
    private readonly createOneTourPort: CreateOneTourPort,
    private readonly createOneReservationPort: CreateOneReservationPort,
  ) {}

  /**
   * global exception 조회하기.
   * - 모든 api에서 공통적으로 받을수 있는 exception 입니다.
   * - 특정 api의 로직에 종속되지 않은 auth, validation, internal server error 등이 있습니다.
   *
   * @tag App
   *
   * @return interal server error
   */
  @Auth('public')
  @TypedException<GlobalException>(400)
  @TypedRoute.Get('/exception')
  getGlobalException(): GlobalException {
    return {
      name: 'GlobalException',
      message: 'internal server error.',
      statusCode: 500,
      stack: undefined as unknown as string, // private
      additional: undefined, // private
    };
  }

  /**
   * 서버 상태 확인하기.
   * - 정상이라면 { status: 'ok' }를 반환합니다.
   *
   * @tag App
   *
   * @return status
   */
  @Auth('public')
  @TypedException<GlobalException>(400)
  @TypedRoute.Get('/health')
  getServerHealth(): GetServerHealthView {
    return {
      status: 'ok',
    };
  }

  /**
   * 테스트를 위한 더미 데이터 생성하기.
   * - 유저(100명): userName = 1 ~ 3은 판매자, 4 ~ 100은 고객. (email: user{id}@example.com,  password: string)
   * - 투어(6개): 판매자 1인당 2개씩.
   * - 예약(300개): 예약일 24.02.01 ~ 24.02.03 사이의 예약 랜덤 생성.
   *
   * @tag App
   *
   * @return status
   */
  @Auth('public')
  @TypedException<GlobalException>(400)
  @TypedRoute.Get('/dummy')
  async generateDummyData(): Promise<GetServerHealthView> {
    for (const [idx] of Array(100).fill(0).entries()) {
      const userName = `user${idx + 1}`;
      const userRole: UserRoleEnum = idx + 1 > 3 ? 'customer' : 'seller';

      await this.signUpUserPort.execute({
        email: `${userName}@example.com`,
        name: userName,
        password: 'string',
        userRole,
      });
    }

    for (const [idx] of Array(6).fill(0).entries()) {
      if (idx === 0) {
        this.clsManager.setItem('currUser', { userId: 1, userRoles: ['seller'] });
      } else if (idx === 2) {
        this.clsManager.setItem('currUser', { userId: 2, userRoles: ['seller'] });
      } else if (idx === 4) {
        this.clsManager.setItem('currUser', { userId: 3, userRoles: ['seller'] });
      }

      await this.createOneTourPort.execute({
        name: `투어 ${idx + 1}`,
        tourHolidays: {
          weekly: [],
          monthly: [this.getRandomNumber(1, 30)],
          specific: [],
        },
      });
    }

    let errCount = 0;
    for (const [] of Array(300).fill(0).entries()) {
      this.clsManager.setItem('currUser', {
        userId: this.getRandomNumber(4, 100),
        userRoles: ['customer'],
      });

      await this.createOneReservationPort
        .execute({
          tourId: this.getRandomNumber(1, 6),
          baseDate: `2024-02-0${this.getRandomNumber(1, 3)}`,
        })
        .catch(() => ++errCount);
    }

    console.log(`[FAIL] count=${errCount}`);

    return {
      status: 'ok',
    };
  }

  private getRandomNumber(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}

interface GetServerHealthView {
  status: 'ok';
}
