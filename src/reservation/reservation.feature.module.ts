import { Module } from '@nestjs/common';
import { CreateOneReservationPort } from './create-one-reservation';
import { FindManyReservationsPort } from './find-many-reservations';
import { FindOneReservationPort } from './find-one-reservation';
import { ReservationController } from './reservation.controller';
import { UpdateOneReservationPort } from './update-one-reservation';

@Module({
  controllers: [ReservationController],
  providers: [
    FindManyReservationsPort,
    FindOneReservationPort,
    CreateOneReservationPort,
    UpdateOneReservationPort,
  ],
})
export class ReservationFeatureModule {}
