import { AppException, ReservationStatusEnum } from '@libs/common';
import { tags } from 'typia';

export interface FindOneReservationData {
  id: number & tags.Minimum<1>;
}

export interface FindOneReservationView {
  id: number;
  uuid: string & tags.Format<'uuid'>;
  baseDate: string & tags.Format<'date'>;
  reservationStatuses: {
    id: number;
    type: ReservationStatusEnum;
    reason: string;
  }[];
  tour: {
    id: number;
    name: string;
  };
}

export class FindOneReservationException extends AppException<
  'reservation notfound.' | 'tour notfound.' | 'permission denied.'
> {}
