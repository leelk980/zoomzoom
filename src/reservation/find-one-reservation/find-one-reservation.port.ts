import { D } from '@libs/common';
import { ClsManager } from '@libs/infrastructure/manager';
import { ReservationRepository, TourRepository } from '@libs/infrastructure/repository';
import { Injectable } from '@nestjs/common';
import {
  FindOneReservationData,
  FindOneReservationException,
  FindOneReservationView,
} from './find-one-reservation.type';

@Injectable()
export class FindOneReservationPort {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly reservationRepository: ReservationRepository,
    private readonly tourRepository: TourRepository,
  ) {}

  async execute(data: FindOneReservationData): Promise<FindOneReservationView> {
    const currUser = this.clsManager.getItem('currUser');

    const reservationAgg = await this.reservationRepository.findOneById(data.id);
    if (!reservationAgg) {
      throw new FindOneReservationException('reservation notfound.');
    }
    if (currUser.userRoles.includes('customer') && currUser.userId !== reservationAgg.userId) {
      throw new FindOneReservationException('permission denied.');
    }

    const tourAgg = await this.tourRepository.findOneById(reservationAgg.tourId);
    if (!tourAgg) {
      throw new FindOneReservationException('tour notfound.');
    }
    if (currUser.userRoles.includes('seller') && currUser.userId !== tourAgg.userId) {
      throw new FindOneReservationException('permission denied.');
    }

    return {
      id: reservationAgg.id,
      uuid: reservationAgg.uuid,
      baseDate: D.toDateString(reservationAgg.baseDate),
      reservationStatuses: reservationAgg.reservationStatuses,
      tour: {
        id: tourAgg.id,
        name: tourAgg.name,
      },
    };
  }
}
