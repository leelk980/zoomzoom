import { OmitProperties } from '@libs/common';
import { Auth } from '@libs/middleware/auth.guard';
import { TypedBody, TypedException, TypedParam, TypedQuery, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';
import {
  UpdateOneReservationData,
  UpdateOneReservationException,
  UpdateOneReservationPort,
  UpdateOneReservationView,
} from './update-one-reservation';
import {
  FindOneReservationData,
  FindOneReservationException,
  FindOneReservationPort,
  FindOneReservationView,
} from './find-one-reservation';
import {
  FindManyReservationsData,
  FindManyReservationsException,
  FindManyReservationsPort,
  FindManyReservationsView,
} from './find-many-reservations';
import {
  CreateOneReservationData,
  CreateOneReservationException,
  CreateOneReservationPort,
  CreateOneReservationView,
} from './create-one-reservation';

@Controller('/reservations')
export class ReservationController {
  constructor(
    private readonly findManyReservationsPort: FindManyReservationsPort,
    private readonly findOneReservationPort: FindOneReservationPort,
    private readonly createOneReservationPort: CreateOneReservationPort,
    private readonly updateOneReservationPort: UpdateOneReservationPort,
  ) {}

  /**
   * 예약 신청 목록 조회하기 (seller, customer).
   * - 다른 유저의 예약 신청 목록 조회는 불가능합니다.
   * - 고객 유저라면 본인의 예약 신청 목록만 조회 가능합니다.
   * - 판매자 유저라면 본인의 투어 하위의 예약 신청 목록만 조회 가능합니다.
   * - reservationStatus은 optional이며, 없으면 상태 무관하게 전체 검색합니다.
   *
   * @tag Reservation
   *
   * @security bearer
   *
   * @param data filter, paganation, order 조건
   *
   * @return 예약 신청 목록 & 페이징 정보
   */
  @Auth('seller', 'customer')
  @TypedException<FindManyReservationsException>(400)
  @TypedRoute.Get('/')
  async findManyReservations(
    @TypedQuery() data: FindManyReservationsData,
  ): Promise<FindManyReservationsView> {
    return this.findManyReservationsPort.execute(data);
  }

  /**
   * 예약 신청 상세 조회하기 (seller, customer).
   * - 목록 조회와 마찬가지로 본인의 예약 신청만 조회 가능합니다.
   * - reservationStatuses는 시간순이며, 마지막 항목이 최신입니다.
   *
   * @tag Reservation
   *
   * @security bearer
   *
   * @param id 예약 신청 id
   *
   * @return 예약 신청 상세 정보
   */
  @Auth('seller', 'customer')
  @TypedException<FindOneReservationException>(400)
  @TypedRoute.Get('/:id')
  async findOneReservation(
    @TypedParam('id') id: FindOneReservationData['id'],
  ): Promise<FindOneReservationView> {
    return this.findOneReservationPort.execute({ id });
  }

  /**
   * 예약 신청 생성하기 (customer).
   * - 고객 유저만 예약 신청을 할 수 있습니다.
   * - 예약 신청은 최소 하루전에 가능합니다.
   * - 해당 일자에 승인된 예약이 5개 미만이면 자동 승인되고, 그렇지 않으면 승인 대기 상태입니다.
   *
   * @tag Reservation
   *
   * @security bearer
   *
   * @param data 데이터
   *
   * @return 예약 id
   */
  @Auth('customer')
  @TypedException<CreateOneReservationException>(400)
  @TypedRoute.Post('/')
  async createOneReservation(
    @TypedBody() data: CreateOneReservationData,
  ): Promise<CreateOneReservationView> {
    return this.createOneReservationPort.execute(data);
  }

  /**
   * 예약 신청 수정하기 (seller, customer).
   * - 예약 목록 조회와 마찬가지로 본인의 예약 신청만 변경할 수 있습니다.
   * - 판매자 유저: "pending" | "fail" | "done" 세 가지 상태로 변경 가능합니다.
   * - 고객 유저: "cancel" 상태로만 변경 가능하고, 이는 최소 예약일 3일전에 가능합니다.
   *
   * @tag Reservation
   *
   * @security bearer
   *
   * @param data 데이터
   *
   * @return 예약 id
   */
  @Auth('seller', 'customer')
  @TypedException<UpdateOneReservationException>(400)
  @TypedRoute.Put('/:id')
  async updateOneReservation(
    @TypedParam('id') id: UpdateOneReservationData['id'],
    @TypedBody() data: OmitProperties<UpdateOneReservationData, 'id'>,
  ): Promise<UpdateOneReservationView> {
    return this.updateOneReservationPort.execute({ id, ...data });
  }
}
