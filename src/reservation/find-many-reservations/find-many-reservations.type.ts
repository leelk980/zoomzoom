import { AppException, ReservationStatusEnum } from '@libs/common';
import { tags } from 'typia';

export interface FindManyReservationsData {
  reservationStatus: ReservationStatusEnum | undefined;
  pageIndex: number & tags.Minimum<1>;
  pageSize: number & tags.Minimum<10>;
  sort: 'recent';
}

export interface FindManyReservationsView {
  reservations: {
    id: number;
    uuid: string & tags.Format<'uuid'>;
    baseDate: string & tags.Format<'date'>;
    reservationStatus: ReservationStatusEnum;
    tour: {
      id: number;
      name: string;
    };
  }[];
  pagination: {
    totalElement: number;
    totalPage: number;
  };
}

export class FindManyReservationsException extends AppException<
  'tour notfound.' | 'permission denied.'
> {}
