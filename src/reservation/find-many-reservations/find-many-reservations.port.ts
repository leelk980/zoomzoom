import { D, F } from '@libs/common';
import { ClsManager } from '@libs/infrastructure/manager';
import { ReservationRepository, TourRepository } from '@libs/infrastructure/repository';
import { Injectable } from '@nestjs/common';
import {
  FindManyReservationsData,
  FindManyReservationsException,
  FindManyReservationsView,
} from './find-many-reservations.type';

@Injectable()
export class FindManyReservationsPort {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly reservationRepository: ReservationRepository,
    private readonly tourRepository: TourRepository,
  ) {}

  async execute(data: FindManyReservationsData): Promise<FindManyReservationsView> {
    const { totalElement, reservationAggs, tourIdAggMap } = await (async () => {
      const currUser = this.clsManager.getItem('currUser');

      if (currUser.userRoles.includes('customer')) {
        const totalElement = await this.reservationRepository.count({
          userId: currUser.userId,
          reservationStatus: data.reservationStatus,
        });

        const reservationAggs = await this.reservationRepository.findMany({
          userId: currUser.userId,
          reservationStatus: data.reservationStatus,
          pageIndex: data.pageIndex,
          pageSize: data.pageSize,
          orderBy: data.sort === 'recent' ? { id: 'desc' } : {},
        });

        const tourIdAggMap = F.assoicateBy(
          (it) => it.id,
          await this.tourRepository.findMany({
            id: reservationAggs.map((each) => each.tourId),
            pageIndex: 1,
            pageSize: 100,
            orderBy: { id: 'asc' },
          }),
        );

        return { totalElement, reservationAggs, tourIdAggMap };
      } else if (currUser.userRoles.includes('seller')) {
        const tourAggs = await this.tourRepository.findMany({
          userId: currUser.userId,
          pageIndex: 1,
          pageSize: 100,
          orderBy: { id: 'asc' },
        });

        const totalElement = await this.reservationRepository.count({
          tourId: tourAggs.map((each) => each.id),
          reservationStatus: data.reservationStatus,
        });

        const reservationAggs = await this.reservationRepository.findMany({
          tourId: tourAggs.map((each) => each.id),
          reservationStatus: data.reservationStatus,
          pageIndex: data.pageIndex,
          pageSize: data.pageSize,
          orderBy: data.sort === 'recent' ? { id: 'desc' } : {},
        });

        const tourIdAggMap = F.assoicateBy((it) => it.id, tourAggs);

        return { totalElement, reservationAggs, tourIdAggMap };
      } else {
        throw new FindManyReservationsException('permission denied.');
      }
    })();

    return {
      reservations: reservationAggs.map((each) => {
        const tourAgg = tourIdAggMap[each.tourId];
        if (!tourAgg) {
          throw new FindManyReservationsException('tour notfound.', 400, {
            reservationId: each.id,
            tourId: each.tourId,
          });
        }

        return {
          id: each.id,
          uuid: each.uuid,
          baseDate: D.toDateString(each.baseDate),
          reservationStatus: each.reservationStatuses.slice(-1)[0]?.type || 'pending',
          tour: {
            id: tourAgg.id,
            name: tourAgg.name,
          },
        };
      }),
      pagination: {
        totalElement,
        totalPage: Math.ceil(totalElement / data.pageSize),
      },
    };
  }
}
