import { D } from '@libs/common';
import { ReservationAggregate } from '@libs/domain';
import { ClsManager } from '@libs/infrastructure/manager';
import { ReservationRepository, TourRepository } from '@libs/infrastructure/repository';
import { Transaction } from '@libs/middleware/transaction.aspect';
import { Injectable } from '@nestjs/common';
import { randomUUID } from 'crypto';
import {
  CreateOneReservationData,
  CreateOneReservationException,
  CreateOneReservationView,
} from './create-one-reservation.type';

@Injectable()
export class CreateOneReservationPort {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly tourRepository: TourRepository,
    private readonly reservationRepository: ReservationRepository,
  ) {}

  @Transaction()
  async execute(data: CreateOneReservationData): Promise<CreateOneReservationView> {
    const today = new Date();
    if (D.isBefore(data.baseDate, today) || D.isSame(data.baseDate, today)) {
      throw new CreateOneReservationException('reservation is possible 1 day in advance.');
    }

    const tourAgg = await this.tourRepository.findOneById(data.tourId);
    if (!tourAgg) {
      throw new CreateOneReservationException('tour notfound.');
    }
    if (tourAgg.checkHoliday(new Date(data.baseDate))) {
      throw new CreateOneReservationException('baseDate is on holiday.');
    }

    const newResservataionAgg = ReservationAggregate.create({
      id: 0,
      uuid: randomUUID(),
      baseDate: new Date(data.baseDate),
      tourId: data.tourId,
      userId: this.clsManager.getItem('currUser').userId,
      reservationStatuses: [
        (await this.checkReservationOpen(data))
          ? {
              id: 0,
              type: 'done',
              reason: '(자동) 승인',
            }
          : {
              id: 0,
              type: 'pending',
              reason: '(자동) 인원 초과',
            },
      ],
    });

    const id = await this.reservationRepository.saveOne(newResservataionAgg);

    return { id };
  }

  private async checkReservationOpen(params: CreateOneReservationData) {
    const reservationAggs = await this.reservationRepository.findMany({
      tourId: params.tourId,
      baseDate: new Date(params.baseDate),
      reservationStatus: 'done',
      pageIndex: 1,
      pageSize: 5,
      orderBy: { id: 'asc' },
    });

    if (reservationAggs.length === 5) {
      return false;
    }

    return true;
  }
}
