import { AppException } from '@libs/common';
import { tags } from 'typia';

export interface CreateOneReservationData {
  tourId: number & tags.Minimum<1>;
  baseDate: string & tags.Format<'date'>;
}

export interface CreateOneReservationView {
  id: number;
}

export class CreateOneReservationException extends AppException<
  'tour notfound.' | 'baseDate is on holiday.' | 'reservation is possible 1 day in advance.'
> {}
