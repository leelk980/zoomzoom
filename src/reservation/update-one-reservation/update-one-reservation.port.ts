import { D } from '@libs/common';
import { ClsManager } from '@libs/infrastructure/manager';
import { ReservationRepository, TourRepository } from '@libs/infrastructure/repository';
import { Transaction } from '@libs/middleware/transaction.aspect';
import { Injectable } from '@nestjs/common';
import {
  UpdateOneReservationData,
  UpdateOneReservationException,
  UpdateOneReservationView,
} from './update-one-reservation.type';

@Injectable()
export class UpdateOneReservationPort {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly tourRepository: TourRepository,
    private readonly reservationRepository: ReservationRepository,
  ) {}

  @Transaction()
  async execute(data: UpdateOneReservationData): Promise<UpdateOneReservationView> {
    const reservationAgg = await this.reservationRepository.findOneById(data.id);
    if (!reservationAgg) {
      throw new UpdateOneReservationException('reservation notfound.');
    }
    if (reservationAgg.reservationStatuses.slice(-1)[0]?.type === data.reservationStatus.type) {
      throw new UpdateOneReservationException('already processed.');
    }

    const currUser = this.clsManager.getItem('currUser');
    if (currUser.userRoles.includes('customer')) {
      if (currUser.userId !== reservationAgg.userId || data.reservationStatus.type !== 'cancel') {
        throw new UpdateOneReservationException('permission denied.');
      }

      if (D.isBefore(D.minus(reservationAgg.baseDate, 3, 'days'), new Date())) {
        throw new UpdateOneReservationException('cancellation is possible 3 days in advance.');
      }
    }

    if (currUser.userRoles.includes('seller')) {
      const tourAgg = await this.tourRepository.findOneById(reservationAgg.tourId);
      if (!tourAgg) {
        throw new UpdateOneReservationException('tour notfound.');
      }

      if (currUser.userId !== tourAgg.userId || data.reservationStatus.type === 'cancel') {
        throw new UpdateOneReservationException('permission denied.');
      }
    }

    await this.reservationRepository.saveOne(
      reservationAgg.update({
        reservationStatuses: reservationAgg.reservationStatuses.concat({
          id: 0,
          type: data.reservationStatus.type,
          reason: data.reservationStatus.reason,
        }),
      }),
    );

    return { id: data.id };
  }
}
