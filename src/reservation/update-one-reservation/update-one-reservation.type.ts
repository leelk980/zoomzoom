import { AppException, ReservationStatusEnum } from '@libs/common';
import { tags } from 'typia';

export interface UpdateOneReservationData {
  id: number & tags.Minimum<1>;
  reservationStatus: {
    type: ReservationStatusEnum;
    reason: string;
  };
}

export interface UpdateOneReservationView {
  id: number & tags.Minimum<1>;
}

export class UpdateOneReservationException extends AppException<
  | 'reservation notfound.'
  | 'tour notfound.'
  | 'permission denied.'
  | 'already processed.'
  | 'cancellation is possible 3 days in advance.'
> {}
