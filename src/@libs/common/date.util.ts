import dayjs from 'dayjs';

type Datable = Date | string;

export class D {
  static add(target: Datable, value: number, unit: dayjs.ManipulateType) {
    return dayjs(target).add(value, unit).toDate();
  }

  static minus(target: Datable, value: number, unit: dayjs.ManipulateType) {
    return dayjs(target).subtract(value, unit).toDate();
  }

  static isSame(start: Datable, end: Datable) {
    return dayjs(start).isSame(end);
  }

  static isBefore(start: Datable, end: Datable) {
    return dayjs(start).isBefore(end);
  }

  static isAfter(end: Datable, start: Datable) {
    return dayjs(start).isBefore(end);
  }

  static toDateString(datable: Datable) {
    const dateString = D.toDate(datable).toISOString().split('T')[0];

    if (!dateString) {
      throw new Error(`cannot convert to date string. ${datable}`);
    }

    return dateString;
  }

  /** @return 1 = Monday, ... , 7 = Sunday */
  static getDayCode(datable: Datable) {
    const dayOfWeek = D.toDate(datable).getDay();
    return ((dayOfWeek + 6) % 7) + 1;
  }

  static getDatesInMonth(
    datable: Datable,
    options?: {
      futureOnly: boolean;
    },
  ) {
    const date = D.toDate(datable);
    const targetYear = date.getFullYear();
    const targetMonth = date.getMonth();
    const currentDate = new Date(targetYear, targetMonth, 1);

    const today = new Date();
    const daysInMonth: Date[] = [];
    while (currentDate.getMonth() === targetMonth) {
      if (!options?.futureOnly || currentDate > today) {
        daysInMonth.push(new Date(currentDate));
      }

      currentDate.setDate(currentDate.getDate() + 1);
    }

    return daysInMonth;
  }

  private static toDate(datable: Datable): Date {
    try {
      return typeof datable === 'string' ? new Date(datable) : datable;
    } catch (err) {
      throw new Error(`invalid datable string. value=${datable.toString()}`);
    }
  }
}
