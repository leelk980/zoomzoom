import { D, F, OmitProperties, TourHolidayEnum, Undefinable } from '@libs/common';
import { BaseAggregate, DateProperties } from './base.aggregate';

type TourProperties = OmitProperties<TourAggregate, keyof BaseAggregate<any>, 'methods'>;

export class TourAggregate extends BaseAggregate<'tour'> {
  id!: number;
  name!: string;
  userId!: number;
  tourHolidays!: {
    id: number;
    type: TourHolidayEnum;
    value: string;
  }[];

  static create(params: TourProperties, dates?: DateProperties) {
    const agg = new TourAggregate(params, dates);
    if (agg.id === 0) agg.pushDomainEvent('tour_created');

    return agg;
  }

  update(params: Undefinable<TourProperties>) {
    Object.assign(this, F.removeUndefined(params));
    this.updatedAt = new Date();
    this.pushDomainEvent('tour_updated');

    return this;
  }

  // TODO: test code required.
  checkHoliday(date: Date) {
    for (const { type, value } of this.tourHolidays) {
      if (type === 'weekly' && D.getDayCode(date) === parseInt(value)) {
        return true;
      }

      if (type === 'monthly' && date.getDate() === parseInt(value)) {
        return true;
      }

      if (type === 'specific' && date.getTime() === new Date(value).getTime()) {
        return true;
      }
    }

    return false;
  }
}
