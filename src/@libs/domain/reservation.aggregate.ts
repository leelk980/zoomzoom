import { F, OmitProperties, ReservationStatusEnum, Undefinable } from '@libs/common';
import { BaseAggregate, DateProperties } from './base.aggregate';

type ReservationProperties = OmitProperties<
  ReservationAggregate,
  keyof BaseAggregate<any>,
  'methods'
>;

export class ReservationAggregate extends BaseAggregate<'reservation'> {
  id!: number;
  uuid!: string;
  baseDate!: Date;
  tourId!: number;
  userId!: number;
  reservationStatuses!: {
    id: number;
    type: ReservationStatusEnum;
    reason: string;
  }[];

  static create(params: ReservationProperties, dates?: DateProperties) {
    const agg = new ReservationAggregate(params, dates);
    if (agg.id === 0) agg.pushDomainEvent('reservation_created');

    return agg;
  }

  update(params: Undefinable<ReservationProperties>) {
    Object.assign(this, F.removeUndefined(params));
    this.updatedAt = new Date();
    this.pushDomainEvent('reservation_updated');

    return this;
  }
}
