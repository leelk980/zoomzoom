import { OmitProperties, TourHolidayEnum, UnwrapPromise } from '@libs/common';
import { TourAggregate } from '@libs/domain';
import { Cache } from '@libs/middleware/cache.aspect';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { BaseRepository } from './base.repository';
import { ClsManager } from '../manager';

@Injectable()
export class TourRepository extends BaseRepository<TourAggregate> {
  constructor(protected override readonly clsManager: ClsManager) {
    super();
  }

  protected override includeAll = {
    tour_holiday: { include: { config: true } },
  } satisfies Prisma.tourInclude;

  protected override entity() {
    return this.queryRunner('tour').findFirst({
      include: this.includeAll,
    });
  }

  protected override convertToAgg(entity: UnwrapPromise<ReturnType<typeof this.entity>>) {
    if (!entity) return null;

    return TourAggregate.create(
      {
        id: entity.id,
        name: entity.name,
        userId: entity.user_id,
        tourHolidays: entity.tour_holiday.map((each) => ({
          id: each.id,
          type: each.config.name as TourHolidayEnum,
          value: each.value,
        })),
      },
      {
        createdAt: entity.createdAt,
        updatedAt: entity.updatedAt,
      },
    );
  }

  async count(
    params: OmitProperties<
      Parameters<typeof this.findMany>[0],
      'pageIndex' | 'pageSize' | 'orderBy'
    >,
  ) {
    return this.queryRunner('tour').count({
      where: {
        id: this.wrapCondition(params.id),
        user_id: this.wrapCondition(params.userId),
      },
    });
  }

  async findMany(params: {
    id?: number | number[] | undefined;
    userId?: number | number[] | undefined;
    pageIndex: number;
    pageSize: number;
    orderBy: Prisma.tourOrderByWithRelationInput;
  }) {
    return this.queryRunner('tour')
      .findMany({
        where: {
          id: this.wrapCondition(params.id),
          user_id: this.wrapCondition(params.userId),
        },
        skip: (params.pageIndex - 1) * params.pageSize,
        take: params.pageSize,
        orderBy: params.orderBy,
        include: this.includeAll,
      })
      .then((tours) => tours.map(this.convertToAgg) as TourAggregate[]);
  }

  @Cache({ op: 'set', type: 'tour' })
  async findOneById(id: number): Promise<TourAggregate | null> {
    return this.queryRunner('tour')
      .findFirst({
        where: { id },
        include: this.includeAll,
      })
      .then(this.convertToAgg);
  }

  async findOneByName(name: string): Promise<TourAggregate | null> {
    return this.queryRunner('tour')
      .findFirst({
        where: { name },
        include: this.includeAll,
      })
      .then(this.convertToAgg);
  }

  @Cache({ op: 'evict', type: 'tour', idName: 'id' })
  async saveOne(agg: TourAggregate): Promise<number> {
    if (agg.id !== 0) {
      await this.queryRunner('tour_holiday').deleteMany({
        where: { tour_id: agg.id },
      });

      await this.queryRunner('tour').delete({
        where: { id: agg.id },
      });
    }

    const entity = await this.queryRunner('tour').create({
      data: {
        id: this.wrapPk(agg.id),
        name: agg.name,
        user_id: agg.userId,
        tour_holiday: {
          createMany: {
            data: agg.tourHolidays.map((each) => ({
              id: this.wrapPk(each.id),
              config_id: this.configMap.getId('tour_holiday', each.type),
              value: each.value,
            })),
          },
        },
        createdAt: agg.createdAt,
        updatedAt: agg.updatedAt,
      },
    });

    await this.publishDomainEvent(agg, entity.id);

    return entity.id;
  }
}
