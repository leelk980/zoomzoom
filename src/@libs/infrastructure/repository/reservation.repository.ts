import { OmitProperties, ReservationStatusEnum, UnwrapPromise } from '@libs/common';
import { ReservationAggregate } from '@libs/domain';
import { Cache } from '@libs/middleware/cache.aspect';
import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { BaseRepository } from './base.repository';
import { ClsManager } from '../manager';

@Injectable()
export class ReservationRepository extends BaseRepository<ReservationAggregate> {
  constructor(protected override readonly clsManager: ClsManager) {
    super();
  }

  protected override includeAll = {
    reservation_status: { include: { config: true } },
  } satisfies Prisma.reservationInclude;

  protected override entity() {
    return this.queryRunner('reservation').findFirst({
      include: this.includeAll,
    });
  }

  protected override convertToAgg(entity: UnwrapPromise<ReturnType<typeof this.entity>>) {
    if (!entity) return null;

    return ReservationAggregate.create(
      {
        id: entity.id,
        uuid: entity.uuid,
        baseDate: entity.baseDate,
        tourId: entity.tour_id,
        userId: entity.user_id,
        reservationStatuses: entity.reservation_status.map((each) => ({
          id: each.id,
          type: each.config.name as ReservationStatusEnum,
          reason: each.reason,
        })),
      },
      {
        createdAt: entity.createdAt,
        updatedAt: entity.updatedAt,
      },
    );
  }

  async count(
    params: OmitProperties<
      Parameters<typeof this.findMany>[0],
      'pageIndex' | 'pageSize' | 'orderBy'
    >,
  ) {
    return this.queryRunner('reservation').count({
      where: {
        tour_id: this.wrapCondition(params.tourId),
        baseDate: this.wrapCondition(params.baseDate),
        user_id: this.wrapCondition(params.userId),
        reservation_status: params.reservationStatus
          ? params.reservationStatus === 'pending'
            ? {
                every: { config: { name: 'pending' } },
              }
            : params.reservationStatus === 'fail'
            ? {
                some: { config: { name: 'fail' } },
              }
            : params.reservationStatus === 'done'
            ? {
                some: { config: { name: 'done' } },
                none: { config: { name: 'cancel' } },
              }
            : {
                some: { config: { name: 'cancel' } },
              }
          : {},
      },
    });
  }

  async findMany(params: {
    tourId?: number | number[] | undefined;
    baseDate?: Date | Date[] | undefined;
    userId?: number | number[] | undefined;
    reservationStatus?: ReservationStatusEnum | undefined;
    pageIndex: number;
    pageSize: number;
    orderBy: Prisma.reservationOrderByWithRelationInput;
  }) {
    return this.queryRunner('reservation')
      .findMany({
        where: {
          tour_id: this.wrapCondition(params.tourId),
          baseDate: this.wrapCondition(params.baseDate),
          user_id: this.wrapCondition(params.userId),
          reservation_status: params.reservationStatus
            ? params.reservationStatus === 'pending'
              ? {
                  every: { config: { name: 'pending' } },
                }
              : params.reservationStatus === 'fail'
              ? {
                  some: { config: { name: 'fail' } },
                }
              : params.reservationStatus === 'done'
              ? {
                  some: { config: { name: 'done' } },
                  none: { config: { name: 'cancel' } },
                }
              : {
                  some: { config: { name: 'cancel' } },
                }
            : {},
        },
        skip: (params.pageIndex - 1) * params.pageSize,
        take: params.pageSize,
        orderBy: params.orderBy,
        include: this.includeAll,
      })
      .then((reservations) => reservations.map(this.convertToAgg) as ReservationAggregate[]);
  }

  @Cache({ op: 'set', type: 'reservation' })
  async findOneById(id: number): Promise<ReservationAggregate | null> {
    return this.queryRunner('reservation')
      .findFirst({
        where: { id },
        include: this.includeAll,
      })
      .then(this.convertToAgg);
  }

  @Cache({ op: 'evict', type: 'reservation', idName: 'id' })
  async saveOne(agg: ReservationAggregate): Promise<number> {
    if (agg.id !== 0) {
      await this.queryRunner('reservation_status').deleteMany({
        where: { reservation_id: agg.id },
      });

      await this.queryRunner('reservation').delete({
        where: { id: agg.id },
      });
    }

    const entity = await this.queryRunner('reservation').create({
      data: {
        id: this.wrapPk(agg.id),
        uuid: agg.uuid,
        baseDate: agg.baseDate,
        tour_id: agg.tourId,
        user_id: agg.userId,
        reservation_status: {
          createMany: {
            data: agg.reservationStatuses.map((each) => ({
              id: this.wrapPk(each.id),
              config_id: this.configMap.getId('reservation_status', each.type),
              reason: each.reason,
            })),
          },
        },
        createdAt: agg.createdAt,
        updatedAt: agg.updatedAt,
      },
    });

    await this.publishDomainEvent(agg, entity.id);

    return entity.id;
  }
}
