import { environment } from '@libs/common';
import { Injectable } from '@nestjs/common';
import { Redis } from 'ioredis';

@Injectable()
export class CacheManager {
  private redisClient = new Redis({
    host: environment.cache.host,
    port: environment.cache.port,
  });

  async get(params: { type: string; id: string | number }) {
    const key = `${params.type}-${params.id}`;
    const value = await this.redisClient.get(key);
    if (!value) {
      return null;
    }

    return JSON.parse(value, (_, value) => {
      if (typeof value === 'string') {
        const dateMatch = value.match(/^\d{4}-\d{2}-\d{2}$/);
        const dateTimeMatch = value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);

        if (dateMatch || dateTimeMatch) {
          return new Date(value);
        }
      }

      return value;
    });
  }

  async set(params: { type: string; id: string | number; value: unknown; ttl: number }) {
    const key = `${params.type}-${params.id}`;

    return this.redisClient.set(key, JSON.stringify(params.value), 'EX', params.ttl);
  }

  async delete(params: { type: string; id: string | number }) {
    const key = `${params.type}-${params.id}`;

    return this.redisClient.del(key);
  }
}
