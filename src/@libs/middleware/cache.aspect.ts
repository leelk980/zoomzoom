import { GlobalException } from '@libs/common';
import { ReservationAggregate, TourAggregate, UserAggregate } from '@libs/domain';
import { Aspect, LazyDecorator, WrapParams } from '@toss/nestjs-aop';
import { createDecorator } from '@toss/nestjs-aop';
import { CacheManager } from '../infrastructure/manager';

const CacheSymbol = Symbol('Cache');

type CacheParams = ({ op: 'set'; ttl?: number } | { op: 'evict' }) & {
  type: 'user' | 'tour' | 'reservation';
  idName?: string;
};

@Aspect(CacheSymbol)
export class CacheAspect implements LazyDecorator {
  constructor(private readonly cacheManager: CacheManager) {}

  wrap({ method, metadata }: WrapParams<any, CacheParams>) {
    return async (...params: any[]) => {
      const id = metadata.idName ? params[0][metadata.idName] : params[0];
      if (!id && id !== 0) {
        throw new GlobalException('internal server error.', 500, {
          message: `invalid cache id. property=${metadata.idName}. value=${id}.`,
        });
      }

      if (metadata.op === 'set') {
        const cachedValue = await this.cacheManager.get({
          id,
          type: metadata.type,
        });

        if (cachedValue) {
          console.log('use-cache');
          if (metadata.type === 'user') return UserAggregate.create(cachedValue);
          if (metadata.type === 'tour') return TourAggregate.create(cachedValue);
          if (metadata.type === 'reservation') return ReservationAggregate.create(cachedValue);
        }

        const result = await method(...params);

        console.log('set-cache');
        await this.cacheManager.set({
          id,
          type: metadata.type,
          value: result,
          ttl: metadata.ttl || 60,
        });

        return result;
      }

      if (metadata.op === 'evict') {
        const result = await method(...params);

        if (id !== 0) {
          console.log('evict-cache');
          await this.cacheManager.delete({
            id,
            type: metadata.type,
          });
        }

        return result;
      }
    };
  }
}

export const Cache = (params: CacheParams) => createDecorator(CacheSymbol, params);
