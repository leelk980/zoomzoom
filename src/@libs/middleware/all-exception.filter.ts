import { AppException, GlobalException } from '@libs/common';
import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Response } from 'express';

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<Response>();

    // operational exception
    if (exception instanceof AppException) {
      if (exception.additional)
        console.log(`[ERROR] ${exception.message}, ${JSON.stringify(exception.additional)}`);

      return res.status(exception.statusCode).json({
        statusCode: exception.statusCode,
        name: exception.name,
        message: exception.message,
      });
    }

    // nestjs exception
    if (exception instanceof HttpException) {
      return res.status(exception.getStatus()).json({
        statusCode: exception.getStatus(),
        name: exception.name,
        message: exception.message,
      });
    }

    // input validation exception
    if ((exception as any)?.response?.path) {
      return res.status(400).json(new GlobalException('invalid request data.'));
    }

    console.log(`[ERROR] ${exception.message ?? exception}`);

    return res.status(500).json(new GlobalException('internal server error.', 500));
  }
}
