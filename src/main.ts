import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  SwaggerModule.setup('docs', app, require('../swagger.json'));

  await app.listen(3000);

  console.log(`Server is running on 3000.`);
}

bootstrap();
