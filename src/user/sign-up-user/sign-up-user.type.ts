import { AppException, UserRoleEnum } from '@libs/common';
import { tags } from 'typia';

export interface SignUpUserData {
  email: string & tags.Format<'email'>;
  password: string;
  name: string;
  userRole: UserRoleEnum;
}

export interface SignUpUserView {
  id: number;
}

export class SignUpUserException extends AppException<'already email exist.'> {}
