import { UserAggregate } from '@libs/domain';
import { UserRepository } from '@libs/infrastructure/repository';
import { UnitTestBuilder } from '@libs/test/unit-test.builder';
import { anything, when } from 'ts-mockito';
import { SignUpUserPort } from './sign-up-user.port';
import { SignUpUserData, SignUpUserException } from './sign-up-user.type';

describe('SignUpUserSpec', () => {
  const {
    injector: port,
    mocks: [mockUserRepository],
  } = UnitTestBuilder.setInjector(SignUpUserPort).setMocks(UserRepository).build();

  it(`success to signup as customer.`, async () => {
    // given
    const data: SignUpUserData = {
      email: 'test@test.com',
      name: 'test',
      password: 'string',
      userRole: 'customer',
    };

    when(mockUserRepository.findOneByEmail(data.email)).thenResolve(null);
    when(mockUserRepository.saveOne(anything())).thenResolve(999);

    // when
    const result = await port.execute(data);

    // then
    expect(result.id).toEqual(999);
  });

  it(`fail to signup as customer. (email duplicated)`, async () => {
    // given
    const data: SignUpUserData = {
      email: 'test@test.com',
      name: 'test',
      password: 'string',
      userRole: 'customer',
    };

    when(mockUserRepository.findOneByEmail(data.email)).thenResolve(
      UserAggregate.create({
        id: 1234,
        userRoles: [{ id: 56, type: 'customer' }],
        ...data,
      }),
    );
    when(mockUserRepository.saveOne(anything())).thenResolve(999);

    // when
    const result: SignUpUserException = await port.execute(data).catch((err) => err);

    // then
    expect(result.message).toEqual('already email exist.');
  });
});
