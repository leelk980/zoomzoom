import { AppException, UserRoleEnum } from '@libs/common';

// export interface GetUserMeData {}

export interface GetUserMeView {
  id: number;
  name: string;
  userRoles: {
    id: number;
    type: UserRoleEnum;
  }[];
}

export class GetUserMeException extends AppException<'user notfound.'> {}
