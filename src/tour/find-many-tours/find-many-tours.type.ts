import { AppException } from '@libs/common';
import { tags } from 'typia';

export interface FindManyToursData {
  userId: (number & tags.Minimum<1>) | undefined;
  pageIndex: number & tags.Minimum<1>;
  pageSize: number & tags.Minimum<10>;
  sort: 'recent';
}

export interface FindManyToursView {
  tours: {
    id: number;
    name: string;
  }[];
  pagination: {
    totalElement: number;
    totalPage: number;
  };
}

export class FindManyToursException extends AppException<'invalid.'> {}
