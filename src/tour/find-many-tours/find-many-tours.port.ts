import { TourRepository } from '@libs/infrastructure/repository';
import { Injectable } from '@nestjs/common';
import { FindManyToursData, FindManyToursView } from './find-many-tours.type';

@Injectable()
export class FindManyToursPort {
  constructor(private readonly tourRepository: TourRepository) {}

  async execute(data: FindManyToursData): Promise<FindManyToursView> {
    const totalElement = await this.tourRepository.count({ userId: data.userId });
    const tourAggs = await this.tourRepository.findMany({
      userId: data.userId,
      pageIndex: data.pageIndex,
      pageSize: data.pageSize,
      orderBy: data.sort === 'recent' ? { id: 'desc' } : {},
    });

    return {
      tours: tourAggs,
      pagination: {
        totalElement,
        totalPage: Math.ceil(totalElement / data.pageSize),
      },
    };
  }
}
