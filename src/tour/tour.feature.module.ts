import { Module } from '@nestjs/common';
import { CreateOneTourPort } from './create-one-tour';
import { FindManyToursPort } from './find-many-tours';
import { FindOneTourPort } from './find-one-tour';
import { TourController } from './tour.controller';
import { UpdateOneTourPort } from './update-one-tour';

@Module({
  controllers: [TourController],
  providers: [FindManyToursPort, FindOneTourPort, CreateOneTourPort, UpdateOneTourPort],
})
export class TourFeatureModule {}
