import { OmitProperties } from '@libs/common';
import { Auth } from '@libs/middleware/auth.guard';
import { TypedBody, TypedException, TypedParam, TypedQuery, TypedRoute } from '@nestia/core';
import { Controller } from '@nestjs/common';
import {
  UpdateOneTourData,
  UpdateOneTourException,
  UpdateOneTourPort,
  UpdateOneTourView,
} from './update-one-tour';
import {
  FindOneTourData,
  FindOneTourException,
  FindOneTourPort,
  FindOneTourView,
} from './find-one-tour';
import {
  FindManyToursData,
  FindManyToursException,
  FindManyToursPort,
  FindManyToursView,
} from './find-many-tours';
import {
  CreateOneTourData,
  CreateOneTourException,
  CreateOneTourPort,
  CreateOneTourView,
} from './create-one-tour';

@Controller('/tours')
export class TourController {
  constructor(
    private readonly findManyToursPort: FindManyToursPort,
    private readonly findOneTourPort: FindOneTourPort,
    private readonly createOneTourPort: CreateOneTourPort,
    private readonly updateOneTourPort: UpdateOneTourPort,
  ) {}

  /**
   * 투어 리스트 조회하기.
   * - userId는 투어 판매자의 id 입니다.
   * - userId가 없으면 전체 검색입니다.
   *
   * @tag Tour
   *
   * @param data page 정보
   *
   * @return 투어 리스트
   */
  @Auth('public')
  @TypedException<FindManyToursException>(400)
  @TypedRoute.Get('/')
  async findManyTours(@TypedQuery() data: FindManyToursData): Promise<FindManyToursView> {
    return this.findManyToursPort.execute(data);
  }

  /**
   * 투어 상세 조회하기.
   * - baseMonth는 응답의 availableDates를 위한 기준월입니다.
   *
   * @tag Tour
   *
   * @param id 투어 id
   *
   * @param data 예약 가능일 기준월
   *
   * @return 투어 정보
   */
  @Auth('public')
  @TypedException<FindOneTourException>(400)
  @TypedRoute.Get('/:id')
  async findOneTour(
    @TypedParam('id') id: FindOneTourData['id'],
    @TypedQuery() data: OmitProperties<FindOneTourData, 'id'>,
  ): Promise<FindOneTourView> {
    return this.findOneTourPort.execute({ id, ...data });
  }

  /**
   * 투어 생성하기 (seller).
   * - 판매자 유저만 투어 생성이 가능합니다.
   * - weekly = 매주 X요일. ex) 1=월요일, 2=화요일 ... 7=일요일
   * - monthly = 매달 X일. ex) 1~31 사이 값
   * - speicific = 특정일 ex) 2023-12-12
   *
   * @tag Tour
   *
   * @security bearer
   *
   * @param data 이름, 휴일 정보
   *
   * @return 투어 id
   */
  @Auth('seller')
  @TypedException<CreateOneTourException>(400)
  @TypedRoute.Post('/')
  async createOneTour(@TypedBody() data: CreateOneTourData): Promise<CreateOneTourView> {
    return this.createOneTourPort.execute(data);
  }

  /**
   * 투어 수정하기 (seller).
   * - 해당 투어의 소유자만 수정할 수 있습니다.
   * - 휴일값의 개념은 투어 생성 API와 동일합니다.
   * - id는 optional이며, id가 포함되지 않으면 새로운 휴일을 생성합니다.
   *
   * @tag Tour
   *
   * @security bearer
   *
   * @param id 투어 id
   *
   * @param data 수정할 내용
   *
   * @return 투어 id
   */
  @Auth('seller')
  @TypedException<UpdateOneTourException>(400)
  @TypedRoute.Put('/:id')
  async updateOneTour(
    @TypedParam('id') id: UpdateOneTourData['id'],
    @TypedBody() data: OmitProperties<UpdateOneTourData, 'id'>,
  ): Promise<UpdateOneTourView> {
    return this.updateOneTourPort.execute({ id, ...data });
  }
}
