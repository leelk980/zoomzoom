import { D, F } from '@libs/common';
import { TourRepository, UserRepository } from '@libs/infrastructure/repository';
import { Injectable } from '@nestjs/common';
import { FindOneTourData, FindOneTourException, FindOneTourView } from './find-one-tour.type';

@Injectable()
export class FindOneTourPort {
  constructor(
    private readonly tourRepository: TourRepository,
    private readonly userRepository: UserRepository,
  ) {}

  async execute(data: FindOneTourData): Promise<FindOneTourView> {
    const tourAgg = await this.tourRepository.findOneById(data.id);
    if (!tourAgg) {
      throw new FindOneTourException('tour notfound.');
    }

    const userAgg = await this.userRepository.findOneById(tourAgg.userId);
    if (!userAgg) {
      throw new FindOneTourException('seller notfound.');
    }

    return {
      id: tourAgg.id,
      name: tourAgg.name,
      tourHolidays: F.pipe(
        tourAgg.tourHolidays,
        F.groupBy((it) => it.type, ['weekly', 'monthly', 'specific']),
        F.mapValues(({ key, value }) =>
          value.map((each) => ({
            id: each.id,
            type: each.type,
            value: (key !== 'specific' ? parseInt(each.value) : D.toDateString(each.value)) as any, // TODO: type-safe
          })),
        ),
      ),
      seller: {
        id: userAgg.id,
        name: userAgg.name,
      },
      availableDates: D.getDatesInMonth(data.baseMonth, { futureOnly: true })
        .filter((each) => !tourAgg.checkHoliday(each))
        .map(D.toDateString),
    };
  }
}
