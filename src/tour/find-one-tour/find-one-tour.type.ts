import { AppException, TourHolidayEnum } from '@libs/common';
import { tags } from 'typia';

export interface FindOneTourData {
  id: number & tags.Minimum<1>;
  baseMonth: string & tags.Format<'date'>;
}

export interface FindOneTourView {
  id: number;
  name: string;
  tourHolidays: {
    [key in TourHolidayEnum]: {
      id: number;
      value: key extends 'specific' ? string & tags.Format<'date'> : number & tags.Minimum<1>;
    }[];
  };
  seller: {
    id: number;
    name: string;
  };
  availableDates: (string & tags.Format<'date'>)[];
}

export class FindOneTourException extends AppException<'tour notfound.' | 'seller notfound.'> {}
