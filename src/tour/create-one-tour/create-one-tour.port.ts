import { F } from '@libs/common';
import { TourAggregate } from '@libs/domain';
import { ClsManager } from '@libs/infrastructure/manager';
import { TourRepository } from '@libs/infrastructure/repository';
import { Transaction } from '@libs/middleware/transaction.aspect';
import { Injectable } from '@nestjs/common';
import {
  CreateOneTourData,
  CreateOneTourException,
  CreateOneTourView,
} from './create-one-tour.type';

@Injectable()
export class CreateOneTourPort {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly tourRepository: TourRepository,
  ) {}

  @Transaction()
  async execute(data: CreateOneTourData): Promise<CreateOneTourView> {
    const tour = await this.tourRepository.findOneByName(data.name);
    if (tour) {
      throw new CreateOneTourException('already name exist.', 400, { name: data.name });
    }

    const newTourAgg = TourAggregate.create({
      id: 0,
      name: data.name,
      userId: this.clsManager.getItem('currUser').userId,
      tourHolidays: F.recordToArray(
        ({ key, value }) =>
          value.map((each) => ({
            id: 0,
            type: key,
            value: each.toString(),
          })),
        data.tourHolidays,
      ).flat(),
    });

    const newTourId = await this.tourRepository.saveOne(newTourAgg);

    return { id: newTourId };
  }
}
