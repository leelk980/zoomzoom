import { AppException, TourHolidayEnum } from '@libs/common';
import { tags } from 'typia';

export interface CreateOneTourData {
  name: string;
  tourHolidays: {
    [key in TourHolidayEnum]: (key extends 'specific'
      ? string & tags.Format<'date'>
      : number & tags.Minimum<1>)[];
  };
}

export interface CreateOneTourView {
  id: number;
}

export class CreateOneTourException extends AppException<'already name exist.'> {}
