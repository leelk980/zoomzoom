import { F } from '@libs/common';
import { ClsManager } from '@libs/infrastructure/manager';
import { TourRepository } from '@libs/infrastructure/repository';
import { Transaction } from '@libs/middleware/transaction.aspect';
import { Injectable } from '@nestjs/common';
import {
  UpdateOneTourData,
  UpdateOneTourException,
  UpdateOneTourView,
} from './update-one-tour.type';

@Injectable()
export class UpdateOneTourPort {
  constructor(
    private readonly clsManager: ClsManager,
    private readonly tourRepository: TourRepository,
  ) {}

  @Transaction()
  async execute(data: UpdateOneTourData): Promise<UpdateOneTourView> {
    const tourAgg = await this.tourRepository.findOneById(data.id);
    if (!tourAgg || tourAgg.userId !== this.clsManager.getItem('currUser').userId) {
      throw new UpdateOneTourException('tour notfound.');
    }

    tourAgg.update({
      name: data.name,
      tourHolidays:
        data.tourHolidays &&
        F.recordToArray(
          ({ key, value }) =>
            value.map((each) => ({
              id: 0,
              type: key,
              value: each.toString(),
            })),
          data.tourHolidays,
        ).flat(),
    });

    await this.tourRepository.saveOne(tourAgg);

    return { id: data.id };
  }
}
