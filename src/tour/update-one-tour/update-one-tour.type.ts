import { AppException, TourHolidayEnum } from '@libs/common';
import { tags } from 'typia';

export interface UpdateOneTourData {
  id: number & tags.Minimum<1>;
  name: string | undefined;
  tourHolidays:
    | {
        [key in TourHolidayEnum]: {
          id: (number & tags.Minimum<1>) | undefined;
          value: key extends 'specific' ? string & tags.Format<'date'> : number & tags.Minimum<1>;
        }[];
      }
    | undefined;
}

export interface UpdateOneTourView {
  id: number;
}

export class UpdateOneTourException extends AppException<'tour notfound.'> {}
