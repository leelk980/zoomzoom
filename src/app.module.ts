import { InfrastructureModule } from '@libs/infrastructure/infrastructure.module';
import { MiddlewareModule } from '@libs/middleware/middleware.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CreateOneReservationPort } from './reservation/create-one-reservation';
import { ReservationFeatureModule } from './reservation/reservation.feature.module';
import { CreateOneTourPort } from './tour/create-one-tour';
import { TourFeatureModule } from './tour/tour.feature.module';
import { SignUpUserPort } from './user/sign-up-user';
import { UserFeatureModule } from './user/user.feature.module';

@Module({
  imports: [
    InfrastructureModule.forRoot(),
    MiddlewareModule.forRoot(),
    ReservationFeatureModule,
    TourFeatureModule,
    UserFeatureModule,
  ],
  controllers: [AppController],
  providers: [SignUpUserPort, CreateOneReservationPort, CreateOneTourPort],
})
export class AppModule {}
