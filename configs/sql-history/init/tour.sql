-- CREATE TABLE "tour" -----------------------------------------
CREATE TABLE `tour`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
	`createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_tour_name` UNIQUE( `name` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_tour_user_id" --------------------------------
CREATE INDEX `idx_tour_user_id` ON `tour`( `user_id` );-- -------------------------------------------------------------

-- CREATE TABLE "tour_holiday" -----------------------------------------
CREATE TABLE `tour_holiday`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`tour_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	`value` VarChar( 32 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_tour_holiday_config_id" --------------------------
ALTER TABLE `tour_holiday`
	ADD CONSTRAINT `fk_tour_holiday_config_id` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_tour_holiday_tour_id" ----------------------------
ALTER TABLE `tour_holiday`
	ADD CONSTRAINT `fk_tour_holiday_tour_id` FOREIGN KEY ( `tour_id` )
	REFERENCES `tour`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------