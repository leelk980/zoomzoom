-- CREATE TABLE "reservation" -----------------------------------------
CREATE TABLE `reservation`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`uuid` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL, 
	`baseDate` Date NOT NULL,
	`tour_id` Int( 0 ) NOT NULL,
	`user_id` Int( 0 ) NOT NULL,
	`createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `ux_reservation_uuid` UNIQUE( `uuid` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_reservation_tour_id" --------------------------------
CREATE INDEX `idx_reservation_tour_id` ON `reservation`( `tour_id` );-- -------------------------------------------------------------

-- CREATE INDEX "idx_reservation_user_id" --------------------------------
CREATE INDEX `idx_reservation_user_id` ON `reservation`( `user_id` );-- -------------------------------------------------------------

-- CREATE TABLE "reservation_status" -----------------------------------------
CREATE TABLE `reservation_status`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`reservation_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	`reason` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE LINK "fk_reservation_status_config_id" --------------------------
ALTER TABLE `reservation_status`
	ADD CONSTRAINT `fk_reservation_status_config_id` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

-- CREATE LINK "fk_reservation_status_reservation_id" ----------------------------
ALTER TABLE `reservation_status`
	ADD CONSTRAINT `fk_reservation_status_reservation_id` FOREIGN KEY ( `reservation_id` )
	REFERENCES `reservation`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------