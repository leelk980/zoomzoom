-- CREATE TABLE "config" ---------------------------------------
CREATE TABLE `config`( 
	`id` Int( 0 ) NOT NULL,
	`type` Enum( 
		'domainEvent',
		'user_role',
		'tour_holiday',
		'reservation_status'
	) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`name` VarChar( 64 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`description` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`parent_id` Int( 0 ) NULL DEFAULT NULL,
	`createdAt` DateTime NOT NULL,
	`updatedAt` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_config_type" --------------------------------
CREATE INDEX `idx_config_type` ON `config`( `type` );-- -------------------------------------------------------------

-- CREATE INDEX "idx_config_parent_id" --------------------------------
CREATE INDEX `idx_config_parent_id` ON `config`( `parent_id` );-- -------------------------------------------------------------

-- CREATE TABLE "domainEvent" -----------------------------------------
CREATE TABLE `domainEvent`( 
	`id` Int( 0 ) AUTO_INCREMENT NOT NULL,
	`aggregate` Enum( 'user', 'tour', 'reservation' ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`aggregate_id` Int( 0 ) NOT NULL,
	`config_id` Int( 0 ) NOT NULL,
	`status` Enum( 'ready', 'progress', 'done', 'fail' ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	`errorLog` VarChar( 10000 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
	`processedAt` DateTime,
	`createdAt` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;-- -------------------------------------------------------------

-- CREATE INDEX "idx_domainEvent_status" ------------------------------
CREATE INDEX `idx_domainEvent_status` ON `domainEvent`( `status` );-- -------------------------------------------------------------

-- CREATE LINK "fk_domainEvent_config_id" --------------------------
ALTER TABLE `domainEvent`
	ADD CONSTRAINT `fk_domainEvent_config_id` FOREIGN KEY ( `config_id` )
	REFERENCES `config`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;-- -------------------------------------------------------------

INSERT INTO `config` (`id`,`type`,`name`,`description`,`parent_id`,`createdAt`,`updatedAt`) VALUES 
( 1,'domainEvent','user_created','유저생성',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 101,'domainEvent','tour_created','투어생성',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 102,'domainEvent','tour_updated','투어수정',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 201,'domainEvent','reservation_created','예약생성',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 202,'domainEvent','reservation_updated','예약수정',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),

( 1001,'user_role','admin','관리자',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1002,'user_role','seller','판매자',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1003,'user_role','customer','고객',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),

( 1101,'tour_holiday','weekly','매주',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1102,'tour_holiday','monthly','매달',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1103,'tour_holiday','specific','특정날짜',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),

( 1201,'reservation_status','pending','예약대기중',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1202,'reservation_status','fail','예약실패',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1203,'reservation_status','done','예약완료',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' ),
( 1204,'reservation_status','cancel','예약취소',NULL,'2023-12-27 00:00:00','2023-12-27 00:00:00' )
