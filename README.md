### 개발 환경

- IDE: VSCode
- Language: Node.js(v18.15)
- Database: Mysql(v8.2.0)
- Cache: Redis(latest)
- Web application framework: Nest.js(express)
- ORM: Prisma
- 기타 라이브러리: Nestia, Fxts 등

### 실행 방법

- `configs/dotenv/.env.local`의 mysql, redis 연결 정보를 수정해주세요.
- `configs/sql-history` 하위의 sql을 차례대로 실행해주세요.
- `npm run perpare`과 `npm run gen:prisma`을 실행해주세요.
- `npm run dev`로 서버를 실행해주세요. ("Server is running on 3000"가 출력되면 정상입니다.)
- `http://localhost:3000/docs`로 swagger document 확인 가능합니다.

### 추가 사항

1. 회원가입 및 로그인

- 유저 인증을 위해 구현.
- 유저는 '판매자' or '고객' 두 가지로 구분.
- JWT & Bearer 토큰을 조합.
- 요구사항의 토큰 기능 대체.

2. 당일 예약 불가

- 예약일로부터 최소 1일전에 예약 가능.
- 과거 일자의 예약을 막는 것을 구현하다가 추가.

3. 예약 목록 조회 고도화

- 본인의 예약만 조회 가능.(타인의 내역에는 접근 불가)
- 판매자 유저라면 본인의 투어 하위의 예약 신청 목록만 조회 가능.
- 고객 유저라면 본인이 신청했던 예약 신청 목록만 조회 가능.
- 예약 신청의 상태 (대기/미승인/승인/취소) 별로 검색 가능.

### 테스트 시나리오

\*\*만약, 테스트를 위한 더미 데이터 생성이 필요하시면 [GET ]/\_/dummy로 생성 가능합니다.
(더 자세한 설명은 Swagger API 문서 참고 부탁드립니다.)

1. 판매자 유저

- 회원가입 및 로그인
- 자기 정보 조회
- 투어 생성 및 수정
- 투어 목록 조회
- 투어 상세 조회

=> 판매자 유저로 투어를 생성합니다.

2. 고객 유저

- 회원가입 및 로그인
- 자기 정보 조회
- 예약 생성
- 예약 목록 조회
- 예약 상세 조회

=> 고객 유저로 예약을 생성합니다.

3. 판매자 유저

- 로그인
- 예약 목록 조회
- 예약 상세 조회
- 예약 수정

=> 다시 판매자 유저로 예약을 조회하고 상태를 변경합니다.

4. 고객 유저

- 예약 목록 조회
- 예약 상세 조회
- 예약 수정(취소)

=> 마지막으로 고객 유저로 예약을 조회하고 예약을 취소합니다.

### 테스트코드

테스트 코드 예시

- sign-up-user.spec.ts

### ERD

```mermaid
erDiagram
"config" {
    Int id PK
    config_type type
    String name
    String description
    Int parent_id "nullable"
    DateTime createdAt
    DateTime updatedAt
}
"domainEvent" {
    Int id PK
    domainEvent_aggregate aggregate
    Int aggregate_id
    Int config_id FK
    domainEvent_status status
    String errorLog "nullable"
    DateTime processedAt "nullable"
    DateTime createdAt
}
"reservation" {
    Int id PK
    String uuid UK
    DateTime baseDate
    Int tour_id
    Int user_id
    DateTime createdAt
    DateTime updatedAt
}
"reservation_status" {
    Int id PK
    Int reservation_id FK
    Int config_id FK
    String reason
}
"tour" {
    Int id PK
    String name UK
    Int user_id
    DateTime createdAt
    DateTime updatedAt
}
"tour_holiday" {
    Int id PK
    Int tour_id FK
    Int config_id FK
    String value
}
"user" {
    Int id PK
    String email UK
    String password
    String name
    DateTime createdAt
    DateTime updatedAt
}
"user_role" {
    Int id PK
    Int user_id FK
    Int config_id FK
}
"domainEvent" }o--|| "config" : config
"reservation_status" }o--|| "config" : config
"reservation_status" }o--|| "reservation" : reservation
"tour_holiday" }o--|| "config" : config
"tour_holiday" }o--|| "tour" : tour
"user_role" }o--|| "config" : config
"user_role" }o--|| "user" : user
```
